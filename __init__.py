import bpy        
        
bl_info = {
    "name": "Blextender",
    "author": "Balaclava vel. krzysiuup",
    "version": (0, 0, 4),
    "blender": (2, 80, 0),
    "description": "More cool tools",
}        
        
class OT_AddCustomSplitNormalsMulti(bpy.types.Operator):
    bl_idname = "mesh.customdata_custom_split_normals_add_multi"
    bl_label = "Add Custom Split Normals Data for selection"
    bl_description = "Add custom split normals data to all selected objects"
    
    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'
    
    def execute(self, context):
        for o in context.selected_objects:
            if o.type == 'MESH':
                context.view_layer.objects.active = o
                bpy.ops.mesh.customdata_custom_splitnormals_add()
             
        return {'FINISHED'}

class OT_ClearCustomSplitNormalsMulti(bpy.types.Operator):
    bl_idname = "mesh.customdata_custom_split_normals_clear_multi"
    bl_label = "Clear Custom Split Normals Data for selection"
    bl_description = "Clear custom split normals data on all selected objects"
    
    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'
    
    def execute(self, context):
        for o in context.selected_objects:
            if o.type == 'MESH':
                 context.view_layer.objects.active = o
                 bpy.ops.mesh.customdata_custom_splitnormals_clear()
             
        return {'FINISHED'}


# TODO: Make those two functions less redundant?
def sync_uv_layer_selection(*args):
    name = bpy.context.object.data.uv_layers.active.name

    for o in bpy.context.selected_objects:
        for uv_layer in o.data.uv_layers:
            if uv_layer.name == name:
                o.data.uv_layers.active = uv_layer
                break


# def sync_uv_layer_render(*args):
#     layers = bpy.context.object.data.uv_layers
#     active_render_layer = [layer.active_render for layer in layers if layer.active_render][0]
#
#     for o in bpy.context.selected_objects:
#         for uv_layer in o.data.uv_layers:
#             if uv_layer.name == active_render_layer.name:
#                 uv_layer.active_render = active_render_layer.active_render
#                 break
             
             
classes = (
    OT_AddCustomSplitNormalsMulti,
    OT_ClearCustomSplitNormalsMulti,
)


def menu_func(self, context):
    self.layout.separator()
    self.layout.operator(OT_AddCustomSplitNormalsMulti.bl_idname)
    self.layout.operator(OT_ClearCustomSplitNormalsMulti.bl_idname)

             
def register():
    for c in classes:
        bpy.utils.register_class(c)
        
    bpy.types.VIEW3D_MT_object.append(menu_func)

    bpy.msgbus.subscribe_rna(
        key=(bpy.types.MeshUVLoopLayer, "active"),
        owner=bpy,
        args=(),
        notify=sync_uv_layer_selection,
    )

    # bpy.msgbus.subscribe_rna(
    #     key=(bpy.types.MeshUVLoopLayer, "active_render"),
    #     owner=bpy,
    #     args=(),
    #     notify=sync_uv_layer_render,
    # )
        
def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
        
    bpy.types.VIEW3D_MT_object.remove(menu_func)
